


$(document).ready(function() {

  function garlands(){
    let garlandColor;
    let colors = ['rgb(255,0,0)', 'rgb(0,0,255)', 'rgb(255,255,0)', 'rgb(0,128,0)', 'rgb(0,0,139)', 'rgb(139,0,0)', 'rgb(144,238,144)', 'rgb(128,0,128)'];
    let interval;
    let uniqueRandoms = 0;
    let newArrColors = [];
    let newColors = colors.concat(colors);
    let speedRange = 600;
    let modalContainer = $('<div class="modal-container"></div>');

    createGarland(36);

    function createGarland(count){
      for(let i = 0; i < count; i++){
        let garland = $(' <div class="garland transition"></div>');
        $('.garlands').append(garland);
      }
    }

    function makeUniqueRandom() {
      uniqueRandoms = Math.floor((Math.random() * (newColors.length)));
      let arrElement = newColors[uniqueRandoms];

      for(let i = 0; i < 1; i++){
        if(newArrColors[i] == arrElement){
          newArrColors.shift();

          if(newArrColors.length == 0){
            newArrColors.push('rgb(255,255,255)');
          }
        }
        else{
          if(newArrColors.length >= 1){
            newArrColors.shift();
          }

          newArrColors.push(arrElement);
        }
        return newArrColors[i];
      }
    }

    function getRandom(){
      $('.garland').each(function () {
        garlandColor = makeUniqueRandom();

        $(this).css({
          'background-color': garlandColor,
          'box-shadow': '0px 0px 29px 1px '+ garlandColor+'',
          'border': '4px solid rgba(153,153,153,0.2)'
        });
      });
    }

    $('.save').click(function () {
      let garlandCount = $('#garlandCount').val();
      speedRange = $('#speedRange').val();

      $('.pause').removeClass('current');
      $('.start').addClass('current');
      $('.modal-container').remove();

      clearInterval(interval);

      if(garlandCount <= 0 || garlandCount > 100){
        $('.inpErr').css('display', 'block');
        $('.main').append(modalContainer);
      }
      else{
        $('.garland').remove();
        $('.inpErr').css('display', 'none');
        $('.modal-settings').removeClass('active');
        createGarland(garlandCount);
      }

      if(speedRange < 500 && speedRange > 200){
        $('.garland').each(function () {
          $(this).css('transition', 'all 0.25s ease-in');
        })
      }
      else if(speedRange <= 200){
        $('.garland').each(function () {
          $(this).css('transition', 'all 0.1s ease-in');
        })
      }
      else{
        $('.garland').each(function () {
          $(this).css('transition', 'all 0.5s ease-in');
        })
      }
    });

    $('.settings-button').click(function () {
      $('.modal-settings').addClass('active');
      $('.main').append(modalContainer);
    });

    $('.exit-button').click(function () {
      $('.modal-settings').removeClass('active');
      $('.modal-container').remove();
      $('.inpErr').css('display', 'none');
    });

    $('.start').click(function () {
      $(this).removeClass('current');
      $('.pause').addClass('current');

      clearInterval(interval);
      interval = setInterval(() => {
        getRandom()
      }, speedRange);
    });

    $('.pause').click(function () {
      $(this).removeClass('current');
      $('.start').addClass('current');

      clearInterval(interval);
    });

    $('#speedRange').on('input', function () {
      $('#rangeVal').html( $(this).val());
    });
  }

  garlands();

});

$(window).load(function(){

});

$(window).resize(function(){

});
